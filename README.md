# Installation

After cloning the repo with 

```
git clone https://gitlab.com/jhechtf/react-webpack-from-scratch.git`
``` 

move your terminal into the created folder and run 

```
yarn
```

This should install all the dependencies needed.

# Running the dev server

```
yarn dev
```

Should give you ouput that looks something like this after a few seconds

```
$ webpack-dev-server
i ｢wds｣: Project is running at http://0.0.0.0:8080/
i ｢wds｣: webpack output is served from /
```

Navigate to http://localhost:8080/ and you should see the react application up and running.