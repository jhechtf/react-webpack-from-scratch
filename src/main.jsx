import React from 'react';
import ReactDom from 'react-dom';

const App = (props)=> {
    return (
        <div>Hi {props.name || 'you'}</div>
    );
}

ReactDom.render(<App name="Bob"/>, document.querySelector('#app'));
