const path = require('path');

const HtmlPlugin = require('html-webpack-plugin');

const fromRoot = (...args) => path.join(process.cwd(),...args);
const fromSrc = (...args) => fromRoot('src',...args);

module.exports = {
	mode: process.env.NODE_ENV == 'production' ? 'production' : 'development',
	entry: {
		main: fromSrc('main.jsx')
	},
	output: {
		filename: '[name].[hash].js'
	},
	resolve: {
		extensions: ['.js','.jsx']
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				use: ['babel-loader']
			}
		]
	},
	devServer: {
		hot: true,
		host: '0.0.0.0'
	},
	plugins: [
		new HtmlPlugin({
			template: fromRoot('public','index.html'),
			chunks: ['main']
		})
	]
}